import equal from 'fast-deep-equal'
import { AST, MarkupNode } from './parser'

export type Node = Omit<MarkupNode, 'type' | 'startIndex' | 'endIndex'>

export interface NodeInsert {
  op: 'insert'
  id: keyof AST
  node: Node
}

export interface NodeDelete {
  op: 'delete'
  id: keyof AST
}

export interface DeleteEverything {
  op: 'delete_everything'
}

export interface NodeUpdate {
  op: 'update'
  id: keyof AST
  node: Node
}

export type NodeDifference =
  | NodeInsert
  | NodeDelete
  | DeleteEverything
  | NodeUpdate

export function getDifferences(
  previousAST: AST,
  newAST: AST
): NodeDifference[] {
  const currentNodeIds = Object.keys(previousAST)
  const newNodeIds = Object.keys(newAST)

  const toDelete: NodeDelete[] | DeleteEverything[] =
    currentNodeIds.length > 0 &&
    currentNodeIds.some((id) => newNodeIds.includes(id))
      ? currentNodeIds
          .filter((id) => !newNodeIds.includes(id))
          .map((id) => ({ op: 'delete', id }))
      : [{ op: 'delete_everything' }]

  const toInsert: NodeInsert[] = newNodeIds
    .filter((id) => !currentNodeIds.includes(id))
    .sort((idA, idB) => newAST[idA].startIndex - newAST[idB].startIndex)
    .map((id) => ({ op: 'insert', id, node: transformNodeForDiff(newAST[id]) }))

  const toUpdate: NodeUpdate[] = currentNodeIds
    .filter(
      (id) => newNodeIds.includes(id) && !equal(previousAST[id], newAST[id])
    )
    .map((id) => ({ op: 'update', id, node: transformNodeForDiff(newAST[id]) }))

  return (toDelete as NodeDifference[]).concat(toInsert).concat(toUpdate)
}

function transformNodeForDiff({
  type,
  startIndex,
  endIndex,
  ...node
}: MarkupNode): Node {
  return node
}
