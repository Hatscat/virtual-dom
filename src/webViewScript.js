/* eslint-disable unicorn/prefer-query-selector */
/* eslint-disable unicorn/no-null */
/* eslint-disable no-undef */

/**
 * @typedef {import('./virtual-dom').Node} Node
 */

/**
 * @param {string} nodeDiffsJson
 * @returns {void}
 */
export function webViewApplyDiff(nodeDiffsJson) {
  /** @type {import('./virtual-dom').NodeDifference[]} */
  const nodeDiffs = JSON.parse(nodeDiffsJson)

  /**
   * @param {object} obj
   * @param {object} attribs
   * @returns {object}
   */
  function assign(obj, attribs) {
    Object.keys(attribs).forEach((key) => {
      if (obj[key] != null && typeof obj[key] === 'object') {
        assign(obj[key], attribs[key])
      } else if (typeof attribs[key] === 'object') {
        obj[key] = assign(Array.isArray(attribs[key]) ? [] : {}, attribs[key])
      } else {
        obj[key] = attribs[key]
      }
    })

    return obj
  }

  /**
   * @param {HTMLElement} element
   * @param {Node} node
   * @returns {void}
   */
  function amendElement(element, node) {
    if (node.data) {
      element.innerHTML = decodeURI(node.data)
    }
    if (node.attribs) {
      assign(element, node.attribs)
    }
  }

  nodeDiffs.forEach((diff) => {
    try {
      switch (diff.op) {
        case 'delete_everything': {
          while (this.document.body.firstChild) {
            this.document.body.firstChild.remove()
          }
          break
        }
        case 'delete': {
          const element = this.document.getElementById(diff.id)
          if (element) {
            element.remove()
          }
          break
        }
        case 'insert': {
          const parentElement =
            this.document.getElementById(diff.node.parentId) ||
            this.document.body
          /** @type {HTMLElement} */
          const element = this.document.createElement(
            diff.node.tagName || 'span'
          )
          element.id = String(diff.id)
          amendElement(element, diff.node)
          parentElement.append(element)
          break
        }
        case 'update': {
          const element = this.document.getElementById(diff.id)
          if (element) {
            amendElement(element, diff.node)
          }
          break
        }
        default:
          break
      }
    } catch (error) {
      this.postMessage(`ERROR: ${error.stack}`)
    }
  })
}
