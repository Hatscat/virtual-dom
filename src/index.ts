/* eslint-disable no-console */
import {
  parseHTML,
  AST,
  Highlight,
  Selection,
  combineHighlights,
} from './parser'
import { getDifferences } from './virtual-dom'
import { webViewApplyDiff } from './webViewScript'

type RenderIframeWindow = Window & {
  webViewApplyDiff: (nodeDiffsJson: string) => void
}

const state = {
  contentAST: {} as AST,
  highlights: [] as Highlight[],
}
Object.assign(window, state)

addEventListener('load', main)

export async function main(): Promise<void> {
  const htmlPre = document.querySelector('#html_pre') as HTMLPreElement
  const astPre = document.querySelector('#ast_pre') as HTMLPreElement
  const diffsPre = document.querySelector('#diffs_pre') as HTMLPreElement
  const renderIframe = document.querySelector(
    '#render_iframe'
  ) as HTMLIFrameElement

  htmlPre.textContent = getHTMLExample()

  const iframeWindow = injectScriptToIframe(renderIframe)
  if (iframeWindow) {
    iframeWindow.addEventListener('message', handleIframeMessage)
    iframeWindow.document.addEventListener('selectionchange', () =>
      handleSelectionChange(iframeWindow.document)
    )
  }

  Object.assign(window, {
    htmlPre,
    astPre,
    diffsPre,
    renderIframe,
    iframeWindow,
  })

  htmlPre.addEventListener('DOMCharacterDataModified', async () => {
    await convertHTMLToAST(htmlPre, astPre)
    convertASTToDiffs(astPre, diffsPre)
    applyDiffsToIframe(diffsPre, iframeWindow)
  })
  astPre.addEventListener('DOMCharacterDataModified', async () => {
    convertASTToDiffs(astPre, diffsPre)
    applyDiffsToIframe(diffsPre, iframeWindow)
  })
  diffsPre.addEventListener('DOMCharacterDataModified', async () => {
    applyDiffsToIframe(diffsPre, iframeWindow)
  })

  await convertHTMLToAST(htmlPre, astPre)
  convertASTToDiffs(astPre, diffsPre)
  applyDiffsToIframe(diffsPre, iframeWindow)
}

function injectScriptToIframe(
  iframe: HTMLIFrameElement
): RenderIframeWindow | undefined {
  // iframe.srcdoc = `<script>window.webViewApplyDiff=${webViewApplyDiff.toString()}</script>`
  // return iframe.contentWindow as RenderIframeWindow
  if (iframe.contentWindow) {
    return Object.assign(iframe.contentWindow, { webViewApplyDiff })
  }

  return undefined
}

async function convertHTMLToAST(
  srcElement: HTMLPreElement,
  targetElement: HTMLPreElement
): Promise<void> {
  const rawContent = srcElement.textContent
  if (rawContent) {
    try {
      const content = await parseHTML(rawContent)
      targetElement.textContent = JSON.stringify(content, undefined, 2)
    } catch (error) {
      console.error(error)
    }
  }
}

function convertASTToDiffs(
  srcElement: HTMLPreElement,
  targetElement: HTMLPreElement
): void {
  const astJson = srcElement.textContent
  if (astJson) {
    try {
      const ast = JSON.parse(astJson)
      const diffs = getDifferences(state.contentAST, ast)
      state.contentAST = { ...ast }

      targetElement.textContent = JSON.stringify(diffs, undefined, 2)
    } catch (error) {
      console.error(error)
    }
  }
}

function applyDiffsToIframe(
  srcElement: HTMLPreElement,
  iframeWindow: RenderIframeWindow | undefined
): void {
  const diffsJson = srcElement.textContent
  if (diffsJson && iframeWindow) {
    try {
      const diffs = JSON.parse(diffsJson)
      // console.log(iframeWindow.webViewApplyDiff.toString())
      iframeWindow.webViewApplyDiff(JSON.stringify(diffs))
    } catch (error) {
      console.error(error)
    }
  }
}

function handleIframeMessage(data: any): void {
  console.log(data)
}

function handleSelectionChange(iframeDocument: Document): void {
  const selection = iframeDocument.getSelection()
  if (
    selection &&
    selection.anchorNode &&
    selection.anchorNode.parentElement &&
    state.contentAST[selection.anchorNode.parentElement.id] &&
    selection.focusNode &&
    selection.focusNode.parentElement &&
    state.contentAST[selection.focusNode.parentElement.id]
  ) {
    const s: Selection = {
      text: selection.toString(),
      ends: [
        {
          nodeId: selection.anchorNode.parentElement.id,
          offset: selection.anchorOffset,
        },
        {
          nodeId: selection.focusNode.parentElement.id,
          offset: selection.focusOffset,
        },
      ],
    }

    const [start, end] = s.ends
      .map((e) => state.contentAST[e.nodeId].startIndex + e.offset)
      .sort((a, b) => a - b)

    state.highlights.push({
      id: Math.random().toString(36),
      text: s.text,
      start,
      end,
    })

    insertHighlightsInAST()
  }
}

function insertHighlightsInAST(): void {
  const ast = combineHighlights(state.contentAST, state.highlights)

  // const diffs = getDifferences(state.contentAST, ast)
  const diffs = getDifferences(ast, state.contentAST)

  console.log(diffs)

  // @ts-ignore
  // eslint-disable-next-line no-undef
  // astPre.textContent = JSON.stringify(ast, undefined, 2)
  // convertASTToDiffs(astPre, diffsPre)

  // @ts-ignore
  // eslint-disable-next-line no-undef
  diffsPre.textContent = JSON.stringify(diffs, undefined, 2)
  // @ts-ignore
  // eslint-disable-next-line no-undef
  applyDiffsToIframe(diffsPre, iframeWindow)
}

function getHTMLExample(): string {
  return `
<h1>Lorem Ipsum</h1>

<h1>aaaaaaaaaaaa bbbbbbbbbbb&nbsp;ccccccccccc</h1>

<h4>"Neque porro quisquam est qui <em>dolorem ipsum</em> quia dolor sit amet, consectetur, adipisci velit..."</h4>

<hr />

<div>

  <p>
    Lorem ipsum <strong>dolor sit amet</strong>, consectetur adipiscing elit. <a href="https://developer.mozilla.org/fr/docs/Web/HTML/Element/span">Nunc vel neque quam.</a>
  </p>

  <p style="color:red; background-color: black; font: italic small-caps bold 12px/30px Georgia, serif;">
    Integer eget nisi condimentum, rutrum lorem finibus, varius magna.
  </p>

  <img src="https://developer.mozilla.org/static/img/favicon144.png" alt="Visiter le site MDN">

</div>`
}
