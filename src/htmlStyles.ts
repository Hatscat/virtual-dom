export const BASE_FONT_SIZE = 14
export const STYLE_TAG_ID = '__style_tag'

export type TagStyle = { [name: string]: string | number | boolean }

export type StyleSheet = Record<string, TagStyle>

export function generateStyles(
  styleSheet: Partial<StyleSheet> = {},
  baseFontSize = BASE_FONT_SIZE
): StyleSheet {
  return {
    // block tabs
    body: {
      fontSize: baseFontSize,
    },
    div: {},
    ul: {
      paddingLeft: 20,
      marginBottom: baseFontSize,
    },
    ol: {
      paddingLeft: 20,
      marginBottom: baseFontSize,
    },
    iframe: {
      height: 200,
    },
    hr: {
      marginTop: baseFontSize / 2,
      marginBottom: baseFontSize / 2,
      height: 1,
      backgroundColor: '#CCC',
    },
    // inline tabs
    u: { textDecorationLine: 'underline' },
    em: { fontStyle: 'italic' },
    i: { fontStyle: 'italic' },
    b: { fontWeight: 'bold' },
    s: { textDecorationLine: 'line-through' },
    strong: { fontWeight: 'bold' },
    big: { fontSize: baseFontSize * 1.2 },
    small: { fontSize: baseFontSize * 0.8 },
    a: {
      fontSize: baseFontSize,
      textDecorationLine: 'underline',
      color: '#245dc1',
    },
    h1: generateHeadingStyle(baseFontSize, 2, 0.67),
    h2: generateHeadingStyle(baseFontSize, 1.5, 0.83),
    h3: generateHeadingStyle(baseFontSize, 1.17, 1),
    h4: generateHeadingStyle(baseFontSize, 1, 1.33),
    h5: generateHeadingStyle(baseFontSize, 0.83, 1.67),
    h6: generateHeadingStyle(baseFontSize, 0.67, 2.33),
    sub: {
      verticalAlign: 'top',
      fontSize: baseFontSize * 0.8,
      marginTop: baseFontSize / 2,
    },
    sup: {
      verticalAlign: 'top',
      fontSize: baseFontSize * 0.8,
      marginBottom: baseFontSize / 2,
    },
    p: {
      marginTop: baseFontSize,
      marginBottom: baseFontSize,
    },
    ...styleSheet,
  }
}

/**
 * Small utility for generating heading styles
 *
 * @param baseFontSize the basic font size
 * @param fontMultiplier the amount to multiply the font size by
 * @param marginMultiplier the amount to multiply the margin by
 * @returns a style def for a heading
 */
function generateHeadingStyle(
  baseFontSize = BASE_FONT_SIZE,
  fontMultiplier = 1,
  marginMultiplier = 1
): TagStyle {
  return {
    fontSize: Math.round(baseFontSize * fontMultiplier),
    marginTop: Math.round(baseFontSize * fontMultiplier * marginMultiplier),
    marginBottom: Math.round(baseFontSize * fontMultiplier * marginMultiplier),
    fontWeight: 'bold',
  }
}

export function convertStyleSheetToCSS(styleSheet: StyleSheet): string {
  return Object.keys(styleSheet).reduce(
    (css, tagName) =>
      `${css} ${tagName} {${convertTagStyleToCSS(styleSheet[tagName])}}`,
    ''
  )
}

function convertTagStyleToCSS(tagStyle: TagStyle): string {
  return Object.keys(tagStyle).reduce(
    (css, attribName) =>
      `${css} ${attribName.replace(
        /([A-Z])/g,
        (_, char) => `-${char.toLowerCase()}`
      )}: ${
        typeof tagStyle[attribName] === 'number'
          ? `${Math.round(tagStyle[attribName] as number)}px`
          : tagStyle[attribName]
      };`,
    ''
  )
}
