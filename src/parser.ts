import { DomHandler, Parser as HTMLParser } from 'htmlparser2'
import { DataNode, Element } from 'domhandler'
import {
  generateStyles,
  convertStyleSheetToCSS,
  StyleSheet,
  BASE_FONT_SIZE,
} from './htmlStyles'

export type AST = { [id: string]: MarkupNode }

type HTMLNode = DataNode & Element

export interface BaseNode {
  parentId?: keyof AST
  startIndex: number
  endIndex: number
}

export interface TextNode extends BaseNode {
  type: 'text'
  data: string
}

export interface TagNode extends BaseNode {
  type: 'tag'
  tagName: string
  attribs?: { [name: string]: string | object | undefined }
}

export interface StyleNode extends BaseNode {
  type: 'style'
  tagName: 'style'
  data: string
  startIndex: -1
  endIndex: -1
}

export type MarkupNode = TextNode | TagNode | StyleNode

export type Parser = (content: string, options?: ParserOptions) => Promise<AST>

export type ParserOptions = {
  /***
   * Indicates whether the whitespace in text nodes should be normalized
   * (= all whitespace should be replaced with single spaces). The default value is "false".
   */
  normalizeWhitespace?: boolean
  /**
   * Provide your styles for specific HTML tags.
   */
  styleSheet?: StyleSheet
  /**
   * The default style applied to <Text> components.
   */
  baseFontStyle?: { fontSize: number }
}

type SelectionEnd = { nodeId: string; offset: number }

export type Selection = {
  text: string
  ends: [SelectionEnd, SelectionEnd]
}

export type Highlight = {
  id: string
  text: string
  start: number
  end: number
}

const htmlTagsBlackList: ReadonlyArray<string> = [
  'html',
  'head',
  'body',
  'meta',
  'title',
  'link',
  'style',
  'script',
]

export const STYLE_TAG_ID = '_style_tag'

function getNextId(): string {
  getNextId.prototype.count = getNextId.prototype.count
    ? getNextId.prototype.count + 1
    : 1

  return `__id_${getNextId.prototype.count}`
}

// function cloneObject(obj: any): any {
//   const clone = Array.isArray(obj) ? [] : {}
//   Object.keys(obj).forEach((key) => {
//     if (typeof obj[key] === 'object') {
//       clone[key] = cloneObject(obj[key])
//     } else {
//       clone[key] = obj[key]
//     }
//   })

//   return clone
// }

function cloneObject<T extends {}>(obj: T): T {
  return JSON.parse(JSON.stringify(obj))
}

export function combineHighlights(
  ast: AST,
  highlights: ReadonlyArray<Highlight>
): AST {
  return highlights.reduce((nodes, highlight) => {
    const highlightedNodes = findTextNodesInRange(
      ast,
      highlight.start,
      highlight.end
    )

    Object.keys(highlightedNodes).forEach((nodeId) => {
      const textNode = cloneObject(ast[nodeId])
      delete ast[nodeId]

      const highlightNode = createTagNode({
        tagName: 'mark',
        startIndex: textNode.startIndex,
        endIndex: textNode.endIndex,
        parentId: textNode.parentId,
      })
      const highlightNodeId = getNextId()

      textNode.parentId = highlightNodeId

      ast[highlightNodeId] = highlightNode
      ast[getNextId()] = textNode
    })

    return nodes
  }, cloneObject(ast))
}

function findTextNodesInRange(ast: AST, start: number, end: number): AST {
  return Object.keys(ast).reduce((textNodes, nodeId) => {
    const node = ast[nodeId]
    if (node.type !== 'text') return textNodes

    /** ex: <mark>foo <node>bar</node> foobar </mark> */
    const isInsideRange = start < node.startIndex && end > node.endIndex
    /** ex: <node>foo <mark>bar</node> foobar </mark> */
    const isAtRangeStart = start > node.startIndex && start < node.endIndex
    /** ex: <mark>foo <node>bar</mark> foobar</node> */
    const isAtRangeEnd = end > node.startIndex && end < node.endIndex

    if (isInsideRange || isAtRangeStart || isAtRangeEnd) {
      textNodes[nodeId] = node
    }

    return textNodes
  }, {} as AST)
}

function createTagNode(props: Partial<TagNode>): TagNode {
  return {
    type: 'tag',
    tagName: 'div',
    startIndex: 0,
    endIndex: 0,
    ...props,
  }
}

function extractStyle(inlineStyle: string): { [key: string]: string } {
  const regex = /([^:]+)\s*:\s*([^;]+)\s*;?/gm
  const result: { [key: string]: string } = {}
  let matches

  while ((matches = regex.exec(inlineStyle)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (matches.index === regex.lastIndex) {
      regex.lastIndex += 1
    }
    const [, key, value] = matches
    const name = key.trim().replace(/-(\w)/g, (_, char) => char.toUpperCase())
    result[name] = value.trim()
  }

  return result
}

function combineNodes(dom: HTMLNode[], styleSheet: StyleSheet): AST {
  type ReducerT = { markupNodes: AST; parentId: BaseNode['parentId'] }

  const nodeReducer = (
    { markupNodes, parentId }: ReducerT,
    node: HTMLNode
  ): ReducerT => {
    const id = getNextId()

    switch (node.type) {
      case 'text': {
        markupNodes[id] = {
          type: 'text',
          data: encodeURI(node.data),
          parentId,
          startIndex: node.startIndex ?? 0,
          endIndex: node.endIndex ?? 0,
        }
        break
      }
      case 'tag': {
        if (!htmlTagsBlackList.includes(node.name)) {
          const nodeStyle = node.attribs?.style
            ? extractStyle(node.attribs.style)
            : undefined
          const tagStyle = styleSheet[node.name] as object | undefined
          const style =
            nodeStyle || tagStyle
              ? {
                  ...nodeStyle,
                  ...tagStyle,
                }
              : undefined

          markupNodes[id] = {
            type: 'tag',
            tagName: node.name,
            attribs:
              node.attribs || style ? { ...node.attribs, style } : undefined,
            parentId,
            startIndex: node.startIndex ?? 0,
            endIndex: node.endIndex ?? 0,
          }
        }
        if (node.children) {
          ;(node.children as HTMLNode[]).reduce(nodeReducer, {
            markupNodes,
            parentId: id,
          })
        }

        break
      }
      default:
        break
    }

    return { markupNodes, parentId }
  }

  const styleTag: StyleNode = {
    type: 'style',
    tagName: 'style',
    data: `body {font-size: ${BASE_FONT_SIZE}px;}`,
    startIndex: -1,
    endIndex: -1,
  }

  const { markupNodes: nodes } = dom.reduce(nodeReducer, {
    markupNodes: {
      [STYLE_TAG_ID]: styleTag,
    },
    parentId: undefined,
  })

  return nodes
}

export const emptyAST: AST = Object.freeze({})

export const parseHTML: Parser = (html, options) => {
  const styleSheet = generateStyles(
    options?.styleSheet,
    options?.baseFontStyle?.fontSize
  )

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const css = convertStyleSheetToCSS(styleSheet)
  // console.log(css)

  return new Promise((resolve, reject) => {
    const parser = new HTMLParser(
      new DomHandler(
        (error, dom) => {
          const markupNodes = combineNodes(dom as HTMLNode[], styleSheet)

          if (error) {
            reject(error)
          } else {
            resolve(markupNodes)
          }
        },
        {
          withStartIndices: true,
          withEndIndices: true,
          normalizeWhitespace: options?.normalizeWhitespace ?? true,
        }
      ),
      {
        decodeEntities: false,
        xmlMode: false,
      }
    )
    parser.write(html)
    parser.done()
  })
}

// it's better this way because there are too many entities to decode (https://dev.w3.org/html5/html-author/charref)
function encodeHTMLEntities(content: string): string {
  return content.replace(
    /[\n&<>]/g,
    (entity) =>
      ({
        '\n': '<br>',
        '&': '&amp',
        '<': '&lt;',
        '>': '&gt;',
      }[entity as '\n' | '&' | '<' | '>'])
  )
}

export const parsePlainText: Parser = (content, options) =>
  Promise.resolve({
    [getNextId()]: {
      type: 'text',
      data: encodeURI(encodeHTMLEntities(content)),
      startIndex: 0,
      endIndex: content.length,
    },
  })
